package edu.westga.cs1302.exceptions.controllers;

/**
 * This class defines the controller functions for the 
 * 	exceptions application
 * 
 *
 */
public class DogController {

	public void createShowDog(String name, String age, String registrationNumber) {

	}

	public void createWorkingDog(String name, String age, String maximumWorkHours) {

	}

	/**
	 * This method will build a String representation of the 
	 * 	dogs in the collection with one dog included
	 * 	per line
	 * 
	 * @return A String holding a description of each dog, with
	 * 			one dog per line
	 */
	public String getDescription() {
		return null;
	}

}
